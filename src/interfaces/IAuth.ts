
export interface IAuth {
   userID: string;
   iat: string;
}
