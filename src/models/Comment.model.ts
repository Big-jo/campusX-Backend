import mongoose, { Schema, Document, mongo } from 'mongoose';
import { IPostModel, IComment } from 'src/interfaces/IPost';

const commentSchema: Schema = new Schema({
    author: { type: Schema.Types.ObjectId, ref: 'User' },
    parentPost: {type: Schema.Types.ObjectId, ref: 'Post'},
    text: { type: String },
    video: { type: String, default: null },
    image: { type: String, default: null },
    createdAt: { type: String },
});

export default mongoose.model<IComment>('Post', commentSchema);
