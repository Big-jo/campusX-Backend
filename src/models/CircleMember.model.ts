import mongoose, { Schema } from 'mongoose';
import { ICircleMemberModel } from 'src/interfaces/ICircleMembers';

const CircleMemberSchema = new Schema({
    userID: {type: Schema.Types.ObjectId, ref: 'User'},
});

export default mongoose.model<ICircleMemberModel>('CircleMember', CircleMemberSchema);
