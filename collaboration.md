# Collaboration GuideLines

## Folder Structure

- env
- src
- util

## env

This folder contains the environment files for the application

## src

This foler contains the source files for the application, under this folder are the following sub folders.

- entities - This contains classes used by the application

- interfaces - This contains all the interfaces used throughout the enitre application
  
- lib - Contains functions for application functionality

- middleware - This contains all the middleware functions built for this application and used by express
  
- models - Contains all the models
  
- routes - Contains all the routes
  
- shared - Contains functions shared throughout
  
- spec - Contains all the tests files

